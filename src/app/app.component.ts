import { Component } from '@angular/core';
import { tokenKey } from '@angular/core/src/view';
import {LoginService} from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [LoginService]
})
export class AppComponent {
  title = 'atisa-angular';
  public  token;

  constructor(private _loginService: LoginService){}

  ngOnInit(){
    this.token = this._loginService.getToken();
  }
}
