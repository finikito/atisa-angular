import {Injectable} from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
//import {Observable} from "rxjs/Observable";
import { map } from 'rxjs/operators';

@Injectable()

export class LoginService{
    public  urlLogin = "http://atisa.localhost/checkLogin" ;
    public  urlRegister = "http://atisa.localhost/register" ;
    public token;
    constructor(private _http: HttpClient){}

    signUp(user) {
        let json = JSON.stringify(user);
        let params = json;
        let headers = new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'});
        
        return this._http.post(this.urlLogin,params, {headers:headers});        
    }

    register(user) {
        let json = JSON.stringify(user);
        let params = json;
        let headers = new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'});
        
        return this._http.post(this.urlRegister,params, {headers:headers});        
    }

    /**
     * Recupera el token de acceso del usuario
     */
    getToken(){
        let token = localStorage.getItem('token');
        if(token != "undefined"){
            this.token = token;
        }else{
            this.token = null;
        }

        return this.token;
    }
}