import { Component, OnInit } from '@angular/core';
import {LoginService} from '../services/login.service';
import {Router} from "@angular/router";
import {User} from '../model/user';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public title: string = "Crear cuenta";
  public emailText: string = "Email";
  public nameText: string = "Nombre";
  public passwordText: string = "Contraseña";
  public register: string = "Cree una cuenta";
  public login: string = "Ya tengo cuenta";
  public errorMessage: string;
  public showSuccessMessage:boolean = false ;
  public showErrorMessage:boolean = false ;
  public _errorMessage;
  public user: User;

  constructor(
    private _loginService: LoginService,
    private router: Router,

  ) { }

  ngOnInit() {
    this.user = new User(1,"","","");
  }

  onSubmit(){
    console.log("El usuario tiene ",this.user);
    this._loginService.register(this.user).subscribe(
      response =>{
        //Si la respuesta del servidor es correcta.
        console.log(response);      

       this.showSuccessMessage=true;
       debugger;

      },
      err =>{
        //En caso de error.
        this._errorMessage = <any>err;
        debugger;
        
        if(this._errorMessage != null){
          this.errorMessage = err.error.message;
          this.showErrorMessage = true;
          console.log("Error en la petición ", err);
        }
      }
    );
  }

  goToLogin(){
    this.router.navigate(['/login']);
  }

}
