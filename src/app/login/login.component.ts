import { Component, OnInit } from '@angular/core';
import {LoginService} from '../services/login.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})

export class LoginComponent implements OnInit {

  public title: string = "Acceder";
  public emailText: string = "Email";
  public passwordText: string = "Contraseña";
  public register: string = "Cree una cuenta";  
  public showErrorMessage:boolean = false ;
  public errorMessage: string;
  public _errorMessage;
  public user;
  constructor(
    private _loginService: LoginService,
    private router: Router
    ) {
    
   }

  ngOnInit() {
    //Comprobamos si ya se ha iniciado sesión para cerrarla.
    this.logout();


    this.user ={
      "email":"",
      "password": "",
    };
  }

  onSubmit(){
    console.log(this.user);
    this._loginService.signUp(this.user).subscribe(
      response =>{
        //Si la respuesta del servidor es correcta.
        console.log(response);
        let token = response["token"];

        if(token.length <= 0){
          this.errorMessage = err.error.message;
          this.showErrorMessage = true;
          console.log("Error en el servidor");
        }
        else{
          //En caso de que esté todo correcto almacenamos el token de sesión del usuario
          if(!token.status){
            localStorage.setItem('token',token);
            console.log(localStorage.getItem('token'));
            this.router.navigate(['/']);

          }
        
        }
        

      },
      err =>{
        //En caso de error.
        this._errorMessage = <any>err;

        if(this._errorMessage != null){
          this.errorMessage = err.error.message;
          this.showErrorMessage = true;
          console.log("Error en la petición ", err);
        }
      }
    );
  }

  /*
  En caso de que haya sesión iniciada al accedera al componente la cierra
  */
  logout(){
    let token = this._loginService.getToken();
    if(token != null){
      localStorage.removeItem('token');
    }
  }

  goToRegister(){
    this.router.navigate(['/register']);
  }
  

}
