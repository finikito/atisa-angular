import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.css'],
})
export class AccessComponent implements OnInit {
  public titulo = "Atisa";
  public token;
  constructor(
    private router: Router,
    private _loginService: LoginService
    ) { }


  ngOnInit() {
    this.token = this._loginService.getToken();
  }

  goToLogin(){    
    this.router.navigate(['/login']);
  }

}
